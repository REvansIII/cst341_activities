package controllers;

import models.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class StudentController
{
    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public ModelAndView showForm() {
        return new ModelAndView("studentHome", "student", new Student());
    }

    @RequestMapping(value = "/addStudent", method = RequestMethod.POST)
    public String submit(@ModelAttribute("student") Student student,
                         BindingResult result, ModelMap model)
    {
        if (result.hasErrors())
        {
            return "error";
        }
        model.addAttribute("name", student.getName());
        model.addAttribute("phoneNumber", student.getPhoneNumber());
        model.addAttribute("id", student.getId());
        return "studentView";
    }


}
