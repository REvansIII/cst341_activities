package controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/helloPage")
public class HelloController
{

    @GetMapping
    public ModelAndView helloWorld()
    {
        String message = "Completed Hello World Example";
        message += "<br> Great Job";

        ModelAndView mvc = new ModelAndView();

        mvc.setViewName("helloPage");
        mvc.getModel().put("data", message);

         return mvc;

    //    return new ModelAndView("helloPage","helloMessage", message);
    }
}
