package main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;

@Configuration
@ComponentScan("main")
public class MyConfiguration
{
    @Bean
    public MyService getService()
    {
        return new MyService();

    }
}
