package main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test
{
    public static void main(String[] args)
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(MyConfiguration.class);

        MyService service1 =context.getBean(MyService.class);

        service1.log("CST341 TOPIC 3 ACTIVITY");

        MyService service2 = context.getBean(MyService.class);

        //singleton bean will return same hashCode
        System.out.println("Service Hashcode= "+ service1.hashCode());
        System.out.println("Service2 Hashcode= "+ service2.hashCode());

        context.close();

    }
}
